const String _APP_REST_URL_LOCAL1 = "http://192.168.43.116/siflab/inputapi/";
const String _APP_REST_URL_LOCAL2 = "http://192.168.1.3/siflab/inputapi/";
const String _APP_REST_URL_HOSTD = "https://k3pawd.site/nim/inputapi/";

const int APP_LEVEL_DOSEN = 5;
const int APP_LEVEL_MAHASISWA = 4;

const String APP_REST_URL = _APP_REST_URL_HOSTD;
